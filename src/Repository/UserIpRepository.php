<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\UserIp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserIp>
 *
 * @method UserIp|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserIp|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserIp[]    findAll()
 * @method UserIp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserIpRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserIp::class);
    }

    /**
     * @param UserIp $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserIp $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param UserIp $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserIp $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $userId
     * @param string $ip
     * @return bool
     */
    public function isNewIp(int $userId, string $ip): bool
    {
        try {
            $countRows = $this->createQueryBuilder('u')
                ->select('count(u.id) as count')
                ->andWhere('u.user = :user_id')
                ->setParameters([
                    'user_id' => $userId,
                ])
                ->getQuery()
                ->getSingleScalarResult();
            if ($countRows == 0) {
                return true;
            }

            return !$this->createQueryBuilder('u')
                ->select('u.id')
                ->andWhere('u.user = :user_id')
                ->andWhere('u.ip = :ip')
                ->setParameters([
                    'user_id' => $userId,
                    'ip' => $ip,
                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
        }
        return true;
    }

    /**
     * @param int $userId
     * @param string $ip
     * @return UserIp|null
     */
    public function findByUserIdAndIp(int $userId, string $ip): ?UserIp
    {
        try {
            return $this->createQueryBuilder('u')
                ->andWhere('u.user = :user_id')
                ->andWhere('u.ip = :ip')
                ->setParameters([
                    'user_id' => $userId,
                    'ip' => $ip,
                ])
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException|NonUniqueResultException) {
        }
        return null;
    }
}
