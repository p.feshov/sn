<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\ConfirmationCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ConfirmationCode>
 *
 * @method ConfirmationCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfirmationCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfirmationCode[]    findAll()
 * @method ConfirmationCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfirmationCodeRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfirmationCode::class);
    }

    /**
     * @param ConfirmationCode $entity
     * @param bool $flush
     * @return void
     */
    public function add(ConfirmationCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param ConfirmationCode $entity
     * @param bool $flush
     * @return void
     */
    public function remove(ConfirmationCode $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $type
     * @param string $code
     * @return ?ConfirmationCode
     * @throws NonUniqueResultException
     */
    public function getNonUsedByCode(int $type, string $code): ?ConfirmationCode
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.expiredAt >= CURRENT_TIMESTAMP()')
            ->andWhere('e.type = :type')
            ->andWhere('e.code = :code')
            ->andWhere('e.status = :status')
            ->setParameters([
                'type' => $type,
                'code' => $code,
                'status' => ConfirmationCode::STATUS_ACTIVE
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
