<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\PostLike;
use App\Repository\CacheDecorator\Interfaces\PostLikeRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PostLike>
 *
 * @method PostLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostLike[]    findAll()
 * @method PostLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostLikeRepository extends ServiceEntityRepository implements PostLikeRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostLike::class);
    }

    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function add(PostLike $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function remove(PostLike $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return PostLike|null
     */
    public function findByPostIdAndUserId(int $postId, int $userId): ?PostLike
    {
        try {
            return $this->createQueryBuilder('l')
                ->andWhere('l.post = :post_id')
                ->andWhere('l.user = :user_id')
                ->setParameters([
                    'post_id' => $postId,
                    'user_id' => $userId,
                ])
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException) {
        }
        return null;
    }

    /**
     * @param int $postId
     * @return array
     */
    public function getPostLikes(int $postId): array
    {
        return $this->createQueryBuilder('l')
            ->select('IDENTITY(l.user)')
            ->andWhere('l.post = :post_id')
            ->setParameters([
                'post_id' => $postId
            ])
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param int $postId
     * @return int
     */
    public function getPostLikesCount(int $postId): int
    {
        try {
            return $this->createQueryBuilder('l')
                ->select('count(l.user)')
                ->andWhere('l.post = :post_id')
                ->setParameters([
                    'post_id' => $postId
                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
        }
        return 0;
    }
}
