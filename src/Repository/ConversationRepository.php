<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Conversation;
use App\Entity\Message;
use App\Entity\UserProfile;
use App\Entity\UserRelationship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Conversation>
 *
 * @method Conversation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversation[]    findAll()
 * @method Conversation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversation::class);
    }

    /**
     * @param Conversation $entity
     * @param bool $flush
     * @return void
     */
    public function add(Conversation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Conversation $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Conversation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $userId
     * @return array
     */
    public function findByUserId(int $userId): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('firstname', 'firstName');
        $rsm->addScalarResult('lastname', 'lastName');
        $rsm->addScalarResult('message', 'lastMessage');

        return $this->getEntityManager()
            ->createNativeQuery('
                SELECT c.id, p.firstname, p.lastname,
                       (SELECT substring(text, 1, 150) FROM message m WHERE m.conversation_id = c.id ORDER BY m.created_at LIMIT 1) AS message
                FROM conversation c
                INNER JOIN conversation_user cu ON cu.conversation_id = c.id AND cu.user_id = :user_id
                INNER JOIN conversation_user cu_participant ON cu_participant.conversation_id = c.id AND cu_participant.user_id != :user_id
                INNER JOIN user_profile p ON p.user_id = cu.user_id
                WHERE cu.user_id = :user_id
            ', $rsm)
            ->setParameters([
                'user_id' => $userId,
            ])
            ->getResult();
    }

    /**
     * @param int $userId1
     * @param int $userId2
     * @return bool
     */
    public function isExist(int $userId1, int $userId2): bool
    {
        $conn = $this->getEntityManager()
            ->getConnection();

        $sql = <<<SQL
        select exists(select conversation_user.conversation_id
from conversation_user
         inner join conversation_user s on conversation_user.conversation_id = s.conversation_id and s.user_id = :user_id_1
where conversation_user.user_id = :user_id_2)
SQL;

        try {
            $stmt = $conn->prepare($sql);
            $result = $stmt->executeQuery([
                'user_id_1' => $userId1,
                'user_id_2' => $userId2,
            ]);
            return $result->fetchOne();
        } catch (Exception) {
        }
        return false;
    }
}
