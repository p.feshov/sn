<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\UserRelationship;
use App\Repository\CacheDecorator\Interfaces\UserRelationshipRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserRelationship>
 *
 * @method UserRelationship|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRelationship|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRelationship[]    findAll()
 * @method UserRelationship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRelationshipRepository extends ServiceEntityRepository implements UserRelationshipRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRelationship::class);
    }

    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserRelationship $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserRelationship $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFriends(int $userId): array
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('user_id', 'userId');
        $rsm->addScalarResult('created_at', 'createdAt');

        return $this->getEntityManager()
            ->createNativeQuery('
                SELECT user_first_id as user_id, created_at
                FROM user_relationship
                WHERE user_second_id = :user_id AND status = :status
                UNION ALL
                SELECT user_second_id as user_id, created_at
                FROM user_relationship
                WHERE user_first_id = :user_id AND status = :status;
            ', $rsm)
            ->setParameters([
                'user_id' => $userId,
                'status' => UserRelationship::STATUS_FRIENDS
            ])
            ->getResult();
    }

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return UserRelationship|null
     */
    public function getFriendshipRequest(int $userInitiatorId, int $userReceiverId): ?UserRelationship
    {
        try {
            return $this->createQueryBuilder('u')
                ->andWhere('u.userFirst = :user_id_first')
                ->andWhere('u.userSecond = :user_id_second')
                ->andWhere('u.status = :status')
                ->setParameters([
                    'user_id_first' => $userInitiatorId,
                    'user_id_second' => $userReceiverId,
                    'status' => UserRelationship::STATUS_FRIENDSHIP_REQUEST
                ])
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException) {
        }
        return null;
    }

    /**
     * @param int $userId
     * @param bool $outcome
     * @return array
     */
    public function getFriendshipRequests(int $userId, bool $outcome = false): array
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.createdAt')
            ->andWhere('u.status = :status')
            ->setParameters([
                'user_id' => $userId,
                'status' => UserRelationship::STATUS_FRIENDSHIP_REQUEST
            ]);

        if ($outcome) {
            $qb->addSelect('IDENTITY(u.userSecond) as userId')
                ->andWhere('u.userFirst = :user_id');
        } else {
            $qb->addSelect('IDENTITY(u.userFirst) as userId')
                ->andWhere('u.userSecond = :user_id');
        }

        return $qb
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param int $userIdFirst
     * @param int $userIdSecond
     * @param int|null $status
     * @return UserRelationship|null
     */
    public function getUsersRelation(int $userIdFirst, int $userIdSecond, ?int $status = null): ?UserRelationship
    {
        $params = [
            'user_id_first' => $userIdFirst,
            'user_id_second' => $userIdSecond,
        ];
        try {
            $qb = $this->createQueryBuilder('u');
            $query = $qb
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->andX(
                            $qb->expr()->eq('u.userFirst', ':user_id_first'),
                            $qb->expr()->eq('u.userSecond', ':user_id_second'),
                        ),
                        $qb->expr()->andX(
                            $qb->expr()->eq('u.userFirst', ':user_id_second'),
                            $qb->expr()->eq('u.userSecond', ':user_id_first'),
                        )
                    )
                );

            if ($status) {
                $query
                    ->andWhere('u.status = :status');
                $params['status'] = $status;
            }

            return $query
                ->setParameters($params)
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();
        } catch (NonUniqueResultException) {
        }
        return null;
    }
}
