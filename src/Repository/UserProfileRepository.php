<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\UserProfile;
use App\Repository\CacheDecorator\Interfaces\UserProfileRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserProfile>
 *
 * @method UserProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProfile[]    findAll()
 * @method UserProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProfileRepository extends ServiceEntityRepository implements UserProfileRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserProfile::class);
    }

    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserProfile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserProfile $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $userId
     * @return UserProfile|null
     */
    public function getUserProfile(int $userId): ?UserProfile
    {
        try {
            return $this->createQueryBuilder('u')
                ->andWhere('u.user = :user_id')
                ->setParameter('user_id', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException) {
        }
        return null;
    }
}
