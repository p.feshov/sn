<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Post;
use App\Repository\CacheDecorator\Interfaces\PostRepositoryInterface;
use App\Util\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Post>
 *
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function add(Post $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Post $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Post|null
     */
    public function findByIdAndUserId(int $id, int $userId): ?Post
    {
        try {
            return $this->createQueryBuilder('p')
                ->andWhere('p.id = :id', 'p.user = :user_id')
                ->setParameters([
                    'id' => $id,
                    'user_id' => $userId,
                ])
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException) {
        }
        return null;
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function findByUserId(
        int $userId,
        int $privacyLevel,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator
    {
        $result = $this->createQueryBuilder('p')
            ->andWhere('p.user = :user_id')
            ->andWhere('p.privacyLevel = :privacy_level')
            ->orderBy('p.publishedAt', 'DESC')
            ->addOrderBy('p.id', 'DESC')
            ->setParameters([
                'user_id' => $userId,
                'privacy_level' => $privacyLevel
            ])
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        $count = $this->getTotalCountForUser($userId, $privacyLevel);
        return new Paginator(
            $result,
            $offset,
            $limit,
            $count
        );
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return int
     */
    public function getTotalCountForUser(int $userId, int $privacyLevel): int
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('count(p.id)')
                ->andWhere('p.user = :user_id')
                ->andWhere('p.privacyLevel = :privacy_level')
                ->setParameters([
                    'user_id' => $userId,
                    'privacy_level' => $privacyLevel
                ])
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
        }
        return 0;
    }
}
