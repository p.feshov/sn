<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Message;
use App\Repository\CacheDecorator\Interfaces\MessageRepositoryInterface;
use App\Util\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Message>
 *
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository implements MessageRepositoryInterface
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function add(Message $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Message $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $conversationId
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function getMessagesList(
        int $conversationId,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator
    {
        $result = $this->createQueryBuilder('m')
            ->select(['m.text', 'm.createdAt', 'IDENTITY(m.sender)'])
            ->andWhere('m.conversation = :conversation_id')
            ->setParameter('conversation_id', $conversationId)
            ->orderBy('m.createdAt', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        $count = $this->getMessagesTotalCount($conversationId);
        return new Paginator(
            $result,
            $offset,
            $limit,
            $count
        );
    }

    /**
     * @param int $conversationId
     * @return int
     */
    public function getMessagesTotalCount(int $conversationId): int
    {
        try {
            return $this->createQueryBuilder('m')
                ->select('count(m.id)')
                ->andWhere('m.conversation = :conversation_id')
                ->setParameter('conversation_id', $conversationId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException) {
        }
        return 0;
    }
}
