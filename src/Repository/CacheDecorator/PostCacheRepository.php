<?php

namespace App\Repository\CacheDecorator;

use App\Entity\Post;
use App\Repository\CacheDecorator\Interfaces\PostRepositoryInterface;
use App\Repository\PostRepository;
use App\Util\Paginator;
use App\Util\RedisHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class PostCacheRepository implements PostRepositoryInterface, ServiceEntityRepositoryInterface
{
    protected PostRepository $repository;
    protected RedisHelper $redis;
    protected SerializerInterface $serializer;

    /**
     * @param PostRepository $repository
     * @param RedisHelper $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(
        PostRepository      $repository,
        RedisHelper         $redis,
        SerializerInterface $serializer,
    )
    {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function add(Post $entity, bool $flush = false): void
    {
        $this->repository->add($entity, $flush);
        if ($flush) {
            $this->updateUsersPostsCache(
                $entity->getUser()->getId(),
                $entity->getPrivacyLevel(),
            );
            $this->updateUsersPostCountCache(
                $entity->getUser()->getId(),
                $entity->getPrivacyLevel(),
            );
        }
    }

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Post $entity, bool $flush = false): void
    {
        $this->repository->remove($entity, $flush);
        if ($flush) {
            $this->updateUsersPostsCache(
                $entity->getUser()->getId(),
                $entity->getPrivacyLevel(),
            );
            $this->updateUsersPostCountCache(
                $entity->getUser()->getId(),
                $entity->getPrivacyLevel(),
            );
        }
    }

    /**
     * @param int $id
     * @param int $userId
     * @return Post|null
     */
    public function findByIdAndUserId(int $id, int $userId): ?Post
    {
        return $this->repository->findByIdAndUserId($id, $userId);
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function findByUserId(
        int $userId,
        int $privacyLevel,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator
    {
        $lastPostNumber = $offset + $limit;
        if ($lastPostNumber <= self::CACHE_SIZE) {
            $rows = $this->getUsersPostsCache($userId, $privacyLevel);
            if (!$rows) {
                $rows = $this->updateUsersPostsCache($userId, $privacyLevel);
            }

            if ($offset || $limit)  {
                $rows = array_slice($rows, $offset, $limit);
            }

            return new Paginator(
                $rows,
                $offset,
                $limit,
                $this->getTotalCountForUser($userId, $privacyLevel)
            );
        }
        return $this->repository->findByUserId(
            $userId,
            $privacyLevel,
            $offset,
            $limit
        );
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return int
     */
    public function getTotalCountForUser(int $userId, int $privacyLevel): int
    {
        $count = $this->getUsersPostCountCache($userId, $privacyLevel);
        if (!$count) {
            $count = $this->updateUsersPostCountCache($userId, $privacyLevel);
        }
        return $count;
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return Post[]
     */
    protected function updateUsersPostsCache(int $userId, int $privacyLevel): array
    {
        $paginator = $this->repository->findByUserId($userId, $privacyLevel, 0, self::CACHE_SIZE);
        $rows = $paginator->getItems();
        if (!$rows) {
            return [];
        }

        $rowsJson = $this->serializer->serialize($rows, JsonEncoder::FORMAT, [
            'groups' => 'user_posts',
            AbstractObjectNormalizer::SKIP_NULL_VALUES => true
        ]);
        $this->redis->set(
            static::getUsersPostsCacheKey($userId, $privacyLevel),
            $rowsJson
        );
        return $rows;
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return int
     */
    protected function updateUsersPostCountCache(int $userId, int $privacyLevel): int
    {
        $count = $this->repository->getTotalCountForUser($userId, $privacyLevel);
        $this->redis->set(
            static::getUsersPostCountCacheKey($userId, $privacyLevel),
            $count
        );
        return $count;
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return array
     */
    protected function getUsersPostsCache(int $userId, int $privacyLevel): array
    {
        $rows = $this->redis->get(static::getUsersPostsCacheKey($userId, $privacyLevel));
        if ($rows) {
            return $this->serializer->deserialize($rows, Post::class . '[]', JsonEncoder::FORMAT);
        }
        return [];
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return int|null
     */
    protected function getUsersPostCountCache(int $userId, int $privacyLevel): ?int
    {
        return $this->redis->get(static::getUsersPostCountCacheKey($userId, $privacyLevel));
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return string
     */
    protected static function getUsersPostsCacheKey(int $userId, int $privacyLevel): string
    {
        return "post:$userId:$privacyLevel";
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return string
     */
    protected static function getUsersPostCountCacheKey(int $userId, int $privacyLevel): string
    {
        return "post:count:$userId:$privacyLevel";
    }
}