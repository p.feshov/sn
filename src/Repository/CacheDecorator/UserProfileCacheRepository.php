<?php

namespace App\Repository\CacheDecorator;

use App\Entity\UserProfile;
use App\Repository\CacheDecorator\Interfaces\UserProfileRepositoryInterface;
use App\Repository\UserProfileRepository;
use App\Util\RedisHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class UserProfileCacheRepository implements UserProfileRepositoryInterface, ServiceEntityRepositoryInterface
{
    protected UserProfileRepository $repository;
    protected RedisHelper $redis;
    protected SerializerInterface $serializer;

    /**
     * @param UserProfileRepository $repository
     * @param RedisHelper $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(
        UserProfileRepository $repository,
        RedisHelper           $redis,
        SerializerInterface   $serializer,
    )
    {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserProfile $entity, bool $flush = false): void
    {
        $this->repository->add($entity, $flush);
    }

    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserProfile $entity, bool $flush = false): void
    {
        $this->repository->remove($entity, $flush);
    }

    /**
     * @param int $userId
     * @return UserProfile|null
     */
    public function getUserProfile(int $userId): ?UserProfile
    {
        $row = $this->getUserProfileCache($userId);
        if (!$row) {
            $row = $this->updateUserProfileCache($userId);
        }
        return $row;
    }

    /**
     * @param int $userId
     * @return UserProfile|null
     */
    protected function getUserProfileCache(int $userId): ?UserProfile
    {
        $cache = $this->redis->get(
            static::getUserProfileCacheKey($userId)
        );
        if ($cache) {
            return $this->serializer->deserialize(
                $cache,
                UserProfile::class,
                JsonEncoder::FORMAT
            );
        }
        return null;
    }

    /**
     * @param int $userId
     * @return UserProfile|null
     */
    protected function updateUserProfileCache(int $userId): ?UserProfile {
        $key = static::getUserProfileCacheKey($userId);
        $row = $this->repository->getUserProfile($userId);
        $this->redis->set(
            $key,
            $this->serializer->serialize(
                $row,
                JsonEncoder::FORMAT,
                ['groups' => 'friends']
            )
        );
        return $row;
    }

    /**
     * @param int $userId
     * @return string
     */
    protected static function getUserProfileCacheKey(int $userId): string
    {
        return "profile:$userId";
    }
}