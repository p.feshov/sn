<?php

namespace App\Repository\CacheDecorator\Interfaces;

use App\Entity\Message;
use App\Util\Paginator;

interface MessageRepositoryInterface
{
    const DEFAULT_PAGE_SIZE = 20;
    const CACHE_SIZE = 50;

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function add(Message $entity, bool $flush = false): void;

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Message $entity, bool $flush = false): void;

    /**
     * @param int $conversationId
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function getMessagesList(
        int $conversationId,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator;

    /**
     * @param int $conversationId
     * @return int
     */
    public function getMessagesTotalCount(int $conversationId): int;
}