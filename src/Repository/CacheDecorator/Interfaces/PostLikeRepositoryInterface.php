<?php

namespace App\Repository\CacheDecorator\Interfaces;

use App\Entity\PostLike;

interface PostLikeRepositoryInterface
{
    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function add(PostLike $entity, bool $flush = false): void;

    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function remove(PostLike $entity, bool $flush = false): void;

    /**
     * @param int $postId
     * @param int $userId
     * @return PostLike|null
     */
    public function findByPostIdAndUserId(int $postId, int $userId): ?PostLike;

    /**
     * Получить лайки для записи на стене
     *
     * @param int $postId
     * @return array
     */
    public function getPostLikes(int $postId): array;

    /**
     * Получить количество лайков для записи на стене
     *
     * @param int $postId
     * @return int
     */
    public function getPostLikesCount(int $postId): int;
}