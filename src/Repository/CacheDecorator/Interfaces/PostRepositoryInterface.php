<?php

namespace App\Repository\CacheDecorator\Interfaces;

use App\Entity\Post;
use App\Util\Paginator;

interface PostRepositoryInterface
{
    const DEFAULT_PAGE_SIZE = 20;
    const CACHE_SIZE = 50;

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function add(Post $entity, bool $flush = false): void;

    /**
     * @param Post $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Post $entity, bool $flush = false): void;

    /**
     * @param int $id
     * @param int $userId
     * @return Post|null
     */
    public function findByIdAndUserId(int $id, int $userId): ?Post;

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function findByUserId(
        int $userId,
        int $privacyLevel,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator;

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @return int
     */
    public function getTotalCountForUser(int $userId, int $privacyLevel): int;
}