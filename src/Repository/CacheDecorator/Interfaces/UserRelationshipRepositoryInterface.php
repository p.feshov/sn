<?php

namespace App\Repository\CacheDecorator\Interfaces;

use App\Entity\UserRelationship;

interface UserRelationshipRepositoryInterface
{
    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserRelationship $entity, bool $flush = false): void;

    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserRelationship $entity, bool $flush = false): void;

    /**
     * @param int $userId
     * @return array
     */
    public function getFriends(int $userId): array;

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return UserRelationship|null
     */
    public function getFriendshipRequest(int $userInitiatorId, int $userReceiverId): ?UserRelationship;

    /**
     * @param int $userId
     * @param bool $outcome
     * @return array
     */
    public function getFriendshipRequests(int $userId, bool $outcome = false): array;

    /**
     * @param int $userIdFirst
     * @param int $userIdSecond
     * @param int|null $status
     * @return UserRelationship|null
     */
    public function getUsersRelation(int $userIdFirst, int $userIdSecond, ?int $status = null): ?UserRelationship;
}
