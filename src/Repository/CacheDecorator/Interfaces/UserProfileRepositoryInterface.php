<?php

namespace App\Repository\CacheDecorator\Interfaces;

use App\Entity\UserProfile;

interface UserProfileRepositoryInterface
{
    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserProfile $entity, bool $flush = false): void;

    /**
     * @param UserProfile $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserProfile $entity, bool $flush = false): void;

    /**
     * @param int $userId
     * @return UserProfile|null
     */
    public function getUserProfile(int $userId): ?UserProfile;
}
