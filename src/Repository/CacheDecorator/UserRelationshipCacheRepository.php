<?php
declare(strict_types=1);

namespace App\Repository\CacheDecorator;

use App\Entity\UserRelationship;
use App\Repository\CacheDecorator\Interfaces\UserRelationshipRepositoryInterface;
use App\Repository\UserRelationshipRepository;
use App\Util\RedisHelper;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserRelationshipCacheRepository implements UserRelationshipRepositoryInterface, ServiceEntityRepositoryInterface
{
    protected UserRelationshipRepository $repository;
    protected RedisHelper $redis;
    protected SerializerInterface $serializer;

    /**
     * @param UserRelationshipRepository $repository
     * @param RedisHelper $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(
        UserRelationshipRepository $repository,
        RedisHelper                $redis,
        SerializerInterface        $serializer,
    )
    {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function add(UserRelationship $entity, bool $flush = false): void
    {
        $this->repository->add($entity, $flush);
        if ($flush && $entity->getStatus() == UserRelationship::STATUS_FRIENDS) {
            $this->addFriendToCache(
                $entity->getUserFirstId(),
                $entity->getUserSecondId(),
                $entity->getCreatedAt() ?? new DateTimeImmutable()
            );
        }
    }

    /**
     * @param UserRelationship $entity
     * @param bool $flush
     * @return void
     */
    public function remove(UserRelationship $entity, bool $flush = false): void
    {
        $this->repository->remove($entity, $flush);
        if ($flush && $entity->getStatus() == UserRelationship::STATUS_FRIENDS) {
            $this->removeFriendFromCache(
                $entity->getUserFirstId(),
                $entity->getUserSecondId()
            );
        }
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFriends(int $userId): array
    {
        $rows = $this->getFriendsCache($userId);
        if (is_null($rows)) {
            $rows = $this->updateFriendsCache($userId);
        }
        return $rows;
    }

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return UserRelationship|null
     */
    public function getFriendshipRequest(int $userInitiatorId, int $userReceiverId): ?UserRelationship
    {
        return $this->repository->getFriendshipRequest($userInitiatorId, $userReceiverId);
    }

    /**
     * @param int $userId
     * @param bool $outcome
     * @return array
     */
    public function getFriendshipRequests(int $userId, bool $outcome = false): array
    {
        return $this->repository->getFriendshipRequests($userId, $outcome);
    }

    /**
     * @param int $userIdFirst
     * @param int $userIdSecond
     * @param int|null $status
     * @return UserRelationship|null
     */
    public function getUsersRelation(int $userIdFirst, int $userIdSecond, ?int $status = null): ?UserRelationship
    {
        return $this->repository->getUsersRelation($userIdFirst, $userIdSecond, $status);
    }

    /**
     * @param int $userId
     * @return array|null
     */
    protected function getFriendsCache(int $userId): ?array
    {
        $key = static::getFriendsCacheKey($userId);
        if ($this->redis->exists($key)) {
            return $this->redis->zRange($key);
        }
        return null;
    }

    /**
     * @param int $userId
     * @return array|null
     */
    protected function updateFriendsCache(int $userId): ?array
    {
        $key = static::getFriendsCacheKey($userId);
        $rows = $this->repository->getFriends($userId);

        $friendIds = [];
        $rowsToCache = [];
        foreach ($rows as $row) {
            $friendIds[] = $row['userId'];
            $rowsToCache[] = [
                strtotime($row['createdAt']),
                $row['userId'],
            ];
        }
        $this->redis->zAdd($key, $rowsToCache);
        return $friendIds;
    }

    /**
     * @param int $userIdFirst
     * @param int $userIdSecond
     * @param DateTimeImmutable $createdAt
     * @return void
     */
    protected function addFriendToCache(int $userIdFirst, int $userIdSecond, DateTimeImmutable $createdAt): void
    {
        $this->redis->zAdd(
            static::getFriendsCacheKey($userIdFirst),
            [[$createdAt->getTimestamp(), $userIdSecond]],
        );

        $this->redis->zAdd(
            static::getFriendsCacheKey($userIdSecond),
            [[$createdAt->getTimestamp(), $userIdFirst]],
        );
    }

    /**
     * @param int $userIdFirst
     * @param int $userIdSecond
     * @return void
     */
    protected function removeFriendFromCache(int $userIdFirst, int $userIdSecond): void
    {
        $this->redis->zRem(
            static::getFriendsCacheKey($userIdFirst),
            $userIdSecond
        );

        $this->redis->zRem(
            static::getFriendsCacheKey($userIdSecond),
            $userIdFirst
        );
    }

    /**
     * @param int $userId
     * @return string
     */
    protected static function getFriendsCacheKey(int $userId): string
    {
        return "friends:$userId";
    }
}
