<?php

namespace App\Repository\CacheDecorator;

use App\Entity\PostLike;
use App\Repository\CacheDecorator\Interfaces\PostLikeRepositoryInterface;
use App\Repository\PostLikeRepository;
use App\Util\RedisHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\Serializer\SerializerInterface;

class PostLikeCacheRepository implements PostLikeRepositoryInterface, ServiceEntityRepositoryInterface
{
    protected PostLikeRepository $repository;
    protected RedisHelper $redis;
    protected SerializerInterface $serializer;

    /**
     * @param PostLikeRepository $repository
     * @param RedisHelper $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(
        PostLikeRepository  $repository,
        RedisHelper         $redis,
        SerializerInterface $serializer,
    )
    {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function add(PostLike $entity, bool $flush = false): void
    {
        $this->repository->add($entity, $flush);
        if ($flush) {
            $this->addLikeToCache(
                $entity->getPost()->getId(),
                $entity->getUser()->getId()
            );
        }
    }

    /**
     * @param PostLike $entity
     * @param bool $flush
     * @return void
     */
    public function remove(PostLike $entity, bool $flush = false): void
    {
        $this->repository->remove($entity, $flush);
        if ($flush) {
            $this->removeLikeFromCache(
                $entity->getPost()->getId(),
                $entity->getUser()->getId()
            );
        }
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return PostLike|null
     */
    public function findByPostIdAndUserId(int $postId, int $userId): ?PostLike
    {
        return $this->repository->findByPostIdAndUserId($postId, $userId);
    }

    /**
     * @param int $postId
     * @return array
     */
    public function getPostLikes(int $postId): array
    {
        $rows = $this->getPostLikesCache($postId);
        if (!$rows) {
            $rows = $this->updatePostLikesCache($postId);
        }
        return $rows;
    }

    /**
     * @param int $postId
     * @return int
     */
    public function getPostLikesCount(int $postId): int
    {
        $key = static::getPostLikesCacheKey($postId);
        if ($this->redis->exists($key)) {
            return $this->redis->zCard($key);
        }
        return $this->repository->getPostLikesCount($postId);
    }

    /**
     * @param int $postId
     * @return array
     */
    protected function updatePostLikesCache(int $postId): array
    {
        $rows = $this->repository->getPostLikes($postId);
        $rowsToCache = [];
        foreach ($rows as $row) {
            $rowsToCache[] = [0, $row[1]];
        }
        $this->redis->zAdd(
            static::getPostLikesCacheKey($postId),
            $rowsToCache,
        );
        return $rows;
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return void
     */
    protected function addLikeToCache(int $postId, int $userId): void
    {
        $this->redis->zAdd(
            static::getPostLikesCacheKey($postId),
            [[0, $userId]],
        );
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return void
     */
    protected function removeLikeFromCache(int $postId, int $userId): void
    {
        $this->redis->zRem(
            static::getPostLikesCacheKey($postId),
            $userId
        );
    }

    /**
     * @param int $postId
     * @return array
     */
    protected function getPostLikesCache(int $postId): array
    {
        $rows = $this->redis->get($this->getPostLikesCacheKey($postId));
        if ($rows) {
            return json_decode($rows, true);
        }
        return [];
    }

    /**
     * @param int $postId
     * @return string
     */
    protected static function getPostLikesCacheKey(int $postId): string
    {
        return "post:likes:$postId";
    }
}