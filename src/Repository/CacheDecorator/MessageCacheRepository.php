<?php

namespace App\Repository\CacheDecorator;

use App\Entity\Message;
use App\Repository\CacheDecorator\Interfaces\MessageRepositoryInterface;
use App\Repository\MessageRepository;
use App\Util\Paginator;
use App\Util\RedisHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;

class MessageCacheRepository implements MessageRepositoryInterface, ServiceEntityRepositoryInterface
{
    protected MessageRepository $repository;
    protected RedisHelper $redis;
    protected SerializerInterface $serializer;

    /**
     * @param MessageRepository $repository
     * @param RedisHelper $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(
        MessageRepository   $repository,
        RedisHelper         $redis,
        SerializerInterface $serializer,
    )
    {
        $this->repository = $repository;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function add(Message $entity, bool $flush = false): void
    {
        $this->repository->add($entity, $flush);
        if ($flush) {
            $conversationId = $entity->getConversation()->getId();
            $this->updateMessagesCache($conversationId);
            $this->updateMessagesCountCache($conversationId);
        }
    }

    /**
     * @param Message $entity
     * @param bool $flush
     * @return void
     */
    public function remove(Message $entity, bool $flush = false): void
    {
        $this->repository->remove($entity, $flush);
        if ($flush) {
            $conversationId = $entity->getConversation()->getId();
            $this->updateMessagesCache($conversationId);
            $this->updateMessagesCountCache($conversationId);
        }
    }

    /**
     * @param int $conversationId
     * @param int $offset
     * @param int $limit
     * @return Paginator
     */
    public function getMessagesList(
        int $conversationId,
        int $offset,
        int $limit = self::DEFAULT_PAGE_SIZE
    ): Paginator
    {
        $lastPostNumber = $offset + $limit;
        if ($lastPostNumber <= self::CACHE_SIZE) {
            $rows = $this->getMessagesCache($conversationId);
            if (!$rows) {
                $rows = $this->updateMessagesCache($conversationId);
            }

            if ($offset || $limit)  {
                $rows = array_slice($rows, $offset, $limit);
            }

            return new Paginator(
                $rows,
                $offset,
                $limit,
                $this->getMessagesTotalCount($conversationId)
            );
        }
        return $this->repository->getMessagesList(
            $conversationId,
            $offset,
            $limit
        );
    }

    /**
     * @param int $conversationId
     * @return int
     */
    public function getMessagesTotalCount(int $conversationId): int
    {
        $count = $this->getMessagesCountCache($conversationId);
        if (!$count) {
            $count = $this->updateMessagesCountCache($conversationId);
        }
        return $count;
    }

    /**
     * @param int $conversationId
     * @return array
     */
    protected function getMessagesCache(int $conversationId): array
    {
        $rows = $this->redis->get($conversationId);
        if (!$rows) {
            $rows = $this->updateMessagesCache($conversationId);
        }
        return $rows;
    }

    /**
     * @param int $conversationId
     * @return int|null
     */
    protected function getMessagesCountCache(int $conversationId): ?int
    {
        return $this->redis->get(static::getMessagesCountCacheKey($conversationId));
    }

    /**
     * @param int $conversationId
     * @return array
     */
    protected function updateMessagesCache(int $conversationId): array
    {
        $paginator = $this->repository->getMessagesList(
            $conversationId,
            0,
            self::CACHE_SIZE
        );
        $rows = $paginator->getItems();
        if (!$rows) {
            return [];
        }

        $rowsJson = $this->serializer->serialize(
            $rows,
            JsonEncoder::FORMAT
        );
        $this->redis->set(
            static::getMessagesCacheKey($conversationId),
            $rowsJson
        );
        return $rows;
    }

    /**
     * @param int $conversationId
     * @return int
     */
    protected function updateMessagesCountCache(int $conversationId): int
    {
        $count = $this->repository->getMessagesTotalCount($conversationId);
        $this->redis->set(
            static::getMessagesCountCacheKey($conversationId),
            $count
        );
        return $count;
    }

    /**
     * @param int $conversationId
     * @return string
     */
    protected static function getMessagesCacheKey(int $conversationId): string
    {
        return "messages:$conversationId";
    }

    /**
     * @param int $conversationId
     * @return string
     */
    protected static function getMessagesCountCacheKey(int $conversationId): string
    {
        return "messages:count:$conversationId";
    }
}