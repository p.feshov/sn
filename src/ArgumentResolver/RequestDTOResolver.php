<?php
declare(strict_types=1);

namespace App\ArgumentResolver;

use App\Exception\ValidationException;
use Exception;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestDTOResolver implements ArgumentValueResolverInterface
{
    private ValidatorInterface $validator;
    private SerializerInterface $serializer;

    /**
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ValidatorInterface  $validator,
        SerializerInterface $serializer
    )
    {
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        try {
            $reflection = new ReflectionClass($argument->getType());
            if ($reflection->implementsInterface(RequestDTOInterface::class)) {
                return true;
            }
        } catch (Exception) {
        }

        return false;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return iterable
     * @throws Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $requestContent = $request->getContent();
        if (!$requestContent) {
            throw new BadRequestException('Empty body');
        }

        try {
            $dto = $this->serializer->deserialize(
                $requestContent,
                $argument->getType(),
                JsonEncoder::FORMAT
            );
        } catch (Exception $e) {
            if ($_ENV['APP_ENV'] == 'dev') {
                throw $e;
            }
            throw new BadRequestException('Error while parsing body');
        }

        $errors = $this->validator->validate($dto);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        yield $dto;
    }
}