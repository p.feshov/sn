<?php
declare(strict_types=1);

namespace App\Util;

use Redis;
use RedisException;

class RedisHelper
{
    const MIN_TTL = 1;
    const MAX_TTL = 3600 * 24 * 7;

    private ?Redis $redis = null;
    private string $host;
    private string $password;

    /**
     * @param string $host
     * @param string $password
     * @throws RedisException
     */
    public function __construct(string $host, string $password)
    {
        $this->host = $host;
        $this->password = $password;
        $this->connect();
    }

    /**
     * @param string $key
     * @return false|mixed|Redis|string
     * @throws RedisException
     */
    public function get(string $key): mixed
    {
        return $this->redis->get($key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int|null $ttl
     * @return void
     * @throws RedisException
     */
    public function set(string $key, mixed $value, int $ttl = null): void
    {
        if (is_null($ttl)) {
            $this->redis->set($key, $value);
        } else {
            $this->redis->setex($key, $this->normaliseTtl($ttl), $value);
        }
    }

    /**
     * @param string $key
     * @param array $pairs
     * @return void
     * @throws RedisException
     */
    public function zAdd(string $key, array $pairs): void
    {
        foreach ($pairs as $pair) {
            $this->redis->zAdd($key, $pair[0], $pair[1]);
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     * @throws RedisException
     */
    public function zRem(string $key, mixed $value): void
    {
        $this->redis->zRem($key, $value);
    }

    /**
     * @param string $key
     * @return int
     * @throws RedisException
     */
    public function zCard(string $key): int
    {
        return $this->redis->zCard($key);
    }

    /**
     * @param string $key
     * @param int $start
     * @param int $stop
     * @return array
     * @throws RedisException
     */
    public function zRange(string $key, int $start = 0, int $stop = -1): array
    {
        return $this->redis->zRange($key, $start, $stop);
    }

    /**
     * @param string $key
     * @return bool
     * @throws RedisException
     */
    public function exists(string $key): bool
    {
        return !!$this->redis->exists($key);
    }

    /**
     * The ttl is normalised to be 1 second to 1 hour.
     */
    private function normaliseTtl(int $ttl): float|int
    {
        $ttl = ceil(abs($ttl));

        return ($ttl >= self::MIN_TTL && $ttl <= self::MAX_TTL) ? $ttl : self::MAX_TTL;
    }

    /**
     * Connect only if not connected.
     * @throws RedisException
     */
    private function connect(): void
    {
        if (!$this->redis || $this->redis->ping() != '+PONG') {
            $this->redis = new Redis();
            $this->redis->connect($this->host);
            $this->redis->auth($this->password);
        }
    }
}