<?php
declare(strict_types=1);

namespace App\Util;

class Paginator
{
    protected array $items;
    protected int $offset;
    protected int $pageSize;
    protected ?int $currentPage;
    protected ?int $totalPages;

    /**
     * @param array $rows
     * @param int $offset
     * @param int $pageSize
     * @param int $count
     */
    public function __construct(
        array $rows,
        int $offset,
        int $pageSize,
        int $count = 0
    )
    {
        $this->items = $rows;
        $this->offset = $offset;
        $this->pageSize = $pageSize;
        $this->currentPage = $offset > 0 ? (int) ceil($count / $offset) : 1;
        $this->totalPages = (int) ceil($count / $pageSize);
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return int|null
     */
    public function getCurrentPage(): ?int
    {
        return $this->currentPage;
    }

    /**
     * @return int|null
     */
    public function getTotalPages(): ?int
    {
        return $this->totalPages;
    }
}