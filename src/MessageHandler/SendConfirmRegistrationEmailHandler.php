<?php
declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\ConfirmationCode;
use App\Entity\User;
use App\Message\SendRegistrationConfirmationEmail;
use App\Repository\ConfirmationCodeRepository;
use App\Service\EmailService;
use App\Service\User\UserService;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendConfirmRegistrationEmailHandler implements MessageHandlerInterface
{
    private UserService $userService;
    private EmailService $mailer;
    private string $emailTheme;

    /**
     * @param UserService $userService
     * @param EmailService $mailer
     * @param string $confirmationEmailTheme
     */
    public function __construct(
        UserService                $userService,
        EmailService               $mailer,
        string                     $confirmationEmailTheme
    )
    {
        $this->userService = $userService;
        $this->mailer = $mailer;
        $this->emailTheme = $confirmationEmailTheme;
    }

    /**
     * @param SendRegistrationConfirmationEmail $message
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(SendRegistrationConfirmationEmail $message): void
    {
        $code = $this->userService->createConfirmationCode(
            $message->getUserId(),
            $message->getExpirationPeriod()
        );

        $link = sprintf($message->getConfirmationLink() . '?code=%s', $code);
        $body = "Confirm email: <a>$link</a>";
        $this->mailer->sendEmail($message->getEmail(), $this->emailTheme, $body);
    }
}
