<?php
declare(strict_types=1);

namespace App\MessageHandler;

use App\Message\SendNewIpEmail;
use App\Service\EmailService;
use DateTimeImmutable;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class SendNewIpEmailHandler implements MessageHandlerInterface
{
    private EmailService $mailer;
    private string $emailTheme;

    /**
     * @param EmailService $mailer
     * @param string $newIpEmailTheme
     */
    public function __construct(
        EmailService $mailer,
        string       $newIpEmailTheme
    )
    {
        $this->mailer = $mailer;
        $this->emailTheme = $newIpEmailTheme;
    }

    /**
     * @param SendNewIpEmail $message
     * @return void
     * @throws TransportExceptionInterface
     */
    public function __invoke(SendNewIpEmail $message): void
    {
        $this->sendEmail($message->getEmail(), $message->getIp(), $message->getLoginAt());
    }

    /**
     * @param string $emailTo
     * @param string $ip
     * @param DateTimeImmutable $loginAt
     * @throws TransportExceptionInterface
     */
    private function sendEmail(string $emailTo, string $ip, DateTimeImmutable $loginAt)
    {
        $loginAtFormatted = $loginAt->format('Y-m-d H:i');
        $body = "New ip login: from ip $ip at $loginAtFormatted";

        $this->mailer->sendEmail($emailTo, $this->emailTheme, $body);
    }
}
