<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends HttpException
{
    protected $code = 400;

    protected ConstraintViolationListInterface $errors;

    /**
     * @param ConstraintViolationListInterface $errors
     */
    public function __construct(ConstraintViolationListInterface $errors)
    {
        $this->message = $this->getErrorsList($errors);
        parent::__construct($this->code, $this->message);
    }

    /**
     * @param ConstraintViolationListInterface $errors
     * @return string
     */
    public function getErrorsList(ConstraintViolationListInterface $errors) :string
    {
        $errorList = [];
        /** @var ConstraintViolationInterface $error */
        foreach ($errors as $error) {
            $errorList[$error->getPropertyPath()][] = $error->getMessage();
        }

        return json_encode($errorList);
    }
}