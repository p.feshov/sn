<?php

namespace App\Message;

interface MessageWithEmailInterface
{
    public function getEmail(): string;
}