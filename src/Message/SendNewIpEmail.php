<?php
declare(strict_types=1);

namespace App\Message;

use DateTimeImmutable;

final class SendNewIpEmail implements MessageWithEmailInterface
{
    private string $email;
    private string $ip;
    private DateTimeImmutable $loginAt;

    /**
     * @param string $email
     * @param string $ip
     * @param DateTimeImmutable $loginAt
     */
    public function __construct(string $email, string $ip, DateTimeImmutable $loginAt)
     {
         $this->email = $email;
         $this->ip = $ip;
         $this->loginAt = $loginAt;
     }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getLoginAt(): DateTimeImmutable
    {
        return $this->loginAt;
    }
}
