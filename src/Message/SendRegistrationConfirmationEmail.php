<?php
declare(strict_types=1);

namespace App\Message;

final class SendRegistrationConfirmationEmail implements MessageWithEmailInterface
{
    private int $userId;
    private string $email;
    private string $expirationPeriod;
    private string $confirmationLink;

    /**
     * @param int $userId
     * @param string $email
     * @param string $expirationPeriod
     * @param string $confirmationLink
     */
    public function __construct(int $userId, string $email, string $expirationPeriod, string $confirmationLink)
    {
        $this->userId = $userId;
        $this->email = $email;
        $this->expirationPeriod = $expirationPeriod;
        $this->confirmationLink = $confirmationLink;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getExpirationPeriod(): string
    {
        return $this->expirationPeriod;
    }

    /**
     * @return string
     */
    public function getConfirmationLink(): string
    {
        return $this->confirmationLink;
    }
}
