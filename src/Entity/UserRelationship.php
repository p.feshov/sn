<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRelationshipRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserRelationshipRepository::class)]
class UserRelationship
{
    const STATUS_FRIENDSHIP_REQUEST = 1;
    const STATUS_FRIENDS = 2;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_first_id', nullable: false)]
    private $userFirst;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_second_id', nullable: false)]
    private $userSecond;

    #[ORM\Column(type: 'smallint')]
    private $status;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private $createdAt;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options: ['default' => 'CURRENT_TIMESTAMP'])]
    #[Groups(['user_posts'])]
    private $updatedAt;

    public function getUserFirst(): ?User
    {
        return $this->userFirst;
    }

    public function setUserFirst(User $userFirst): self
    {
        $this->userFirst = $userFirst;

        return $this;
    }

    public function getUserFirstId(): int
    {
        return $this->getUserFirst()->getId();
    }

    public function getUserSecond(): ?User
    {
        return $this->userSecond;
    }

    public function setUserSecond(User $userSecond): self
    {
        $this->userSecond = $userSecond;

        return $this;
    }

    public function getUserSecondId(): int
    {
        return $this->getUserSecond()->getId();
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
