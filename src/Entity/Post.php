<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\PostRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Driver\FetchUtils;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post
{
    const PRIVACY_LEVEL_ALL = 1; // Доступно всем
    const PRIVACY_LEVEL_FRIENDS = 2; // Доступно только друзьям

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['user_posts'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, fetch: "LAZY")]
    #[ORM\JoinColumn(name: 'user_id', nullable: false)]
    private $user;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options: ['default' => 'CURRENT_TIMESTAMP'])]
    #[Groups(['user_posts'])]
    private $updatedAt;

    #[ORM\Column(type: 'datetimetz_immutable')]
    #[Groups(['user_posts'])]
    private $publishedAt;

    #[ORM\Column(type: 'smallint')]
    #[Groups(['user_posts'])]
    private $privacyLevel;

    #[ORM\OneToMany(mappedBy: 'post', targetEntity: PostLike::class, fetch: "EXTRA_LAZY")]
    private $likes;

    #[ORM\Column(type: 'text')]
    #[Groups(['user_posts'])]
    private $content;

    #[Groups(['user_posts'])]
    private int $likeCount = 0;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(DateTimeImmutable $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getPrivacyLevel(): ?int
    {
        return $this->privacyLevel;
    }

    public function setPrivacyLevel(int $privacyLevel): self
    {
        $this->privacyLevel = $privacyLevel;

        return $this;
    }

    /**
     * @return Collection<int, PostLike>
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(PostLike $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setPost($this);
        }

        return $this;
    }

    public function removeLike(PostLike $like): self
    {
        if ($this->likes->removeElement($like)) {
            // set the owning side to null (unless already changed)
            if ($like->getPost() === $this) {
                $like->setPost(null);
            }
        }

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return int
     */
    public function getLikeCount(): int
    {
        return $this->likeCount;
    }

    /**
     * @param int $likeCount
     * @return Post
     */
    public function setLikeCount(int $likeCount): Post
    {
        $this->likeCount = $likeCount;
        return $this;
    }
}
