<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ConfirmationCodeRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: ConfirmationCodeRepository::class)]
#[UniqueEntity('code')]
class ConfirmationCode
{
    const TYPE_REGISTRATION = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_USED = 2;
    const STATUS_EXPIRED = 3;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'smallint')]
    private $type;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private $code;

    #[ORM\Column(type: 'smallint', insertable: false, options: ['default' => self::STATUS_ACTIVE])]
    private $status;

    #[ORM\Column(type: 'datetimetz_immutable')]
    private $expiredAt;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options:['default' => 'CURRENT_TIMESTAMP'])]
    private $createdAt;

    #[ORM\Column(type: 'datetimetz_immutable', insertable: false, updatable: false, options:['default' => 'CURRENT_TIMESTAMP'])]
    private $updatedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    private User $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getExpiredAt(): ?DateTimeImmutable
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(DateTimeImmutable $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return ConfirmationCode
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }
}
