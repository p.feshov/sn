<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Conversation;
use App\Repository\CacheDecorator\Interfaces\MessageRepositoryInterface;
use App\Repository\ConversationRepository;
use App\Repository\UserRepository;
use App\Util\Paginator;
use LogicException;

class ConversationService
{
    protected UserRepository $userRepository;
    protected ConversationRepository $conversationRepository;
    protected MessageRepositoryInterface $messageRepository;

    public function __construct(
        UserRepository             $userRepository,
        ConversationRepository     $conversationRepository,
        MessageRepositoryInterface $messageRepository,
    )
    {
        $this->userRepository = $userRepository;
        $this->conversationRepository = $conversationRepository;
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getConversationList(int $userId): array
    {
        return $this->conversationRepository->findByUserId($userId);
    }

    /**
     * @param int $creatorId
     * @param int $recipientUserId
     */
    public function startNewConversation(int $creatorId, int $recipientUserId): void
    {
        if ($creatorId == $recipientUserId) {
            throw new LogicException();
        }

        $creator = $this->userRepository->findOneBy(['id' => $creatorId]);
        $recipient = $this->userRepository->findOneBy(['id' => $recipientUserId]);

        if (!$creator || !$recipient) {
            throw new LogicException();
        }

        if ($this->conversationRepository->isExist($creatorId, $recipientUserId)) {
            throw new LogicException();
        }

        $conversation = new Conversation();
        $conversation->setCreator($creator);
        $conversation->addUser($creator);
        $conversation->addUser($recipient);

        $this->conversationRepository->add($conversation, true);
    }

    /**
     * @param int $conversationId
     * @param int|null $page
     * @param int|null $pageSize
     * @return Paginator
     */
    public function getMessages(
        int  $conversationId,
        ?int $page = 1,
        ?int $pageSize = 20
    ): Paginator
    {
        return $this->messageRepository->getMessagesList(
            $conversationId,
            $page,
            $pageSize
        );
    }
}