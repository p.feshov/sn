<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService
{
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $recipientEmail
     * @param string $theme
     * @param string $htmlBody
     * @throws TransportExceptionInterface
     */
    public function sendEmail(string $recipientEmail, string $theme, string $htmlBody): void
    {
        $email = (new Email())
            ->to($recipientEmail)
            ->subject($theme)
            ->html($htmlBody);

        $this->mailer->send($email);
    }
}