<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Entity\UserRelationship;
use App\Repository\CacheDecorator\Interfaces\UserProfileRepositoryInterface;
use App\Repository\CacheDecorator\Interfaces\UserRelationshipRepositoryInterface;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use LogicException;
use Symfony\Component\Security\Core\Security;

class FriendshipService
{
    protected UserRepository $userRepository;
    protected UserRelationshipRepositoryInterface $relationshipRepository;
    protected UserProfileRepositoryInterface $profileRepository;
    protected ObjectManager $em;

    /**
     * @param UserRepository $userRepository
     * @param UserRelationshipRepositoryInterface $relationshipRepository
     * @param UserProfileRepositoryInterface $profileRepository
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        UserRepository                      $userRepository,
        UserRelationshipRepositoryInterface $relationshipRepository,
        UserProfileRepositoryInterface      $profileRepository,
        ManagerRegistry                     $doctrine
    )
    {
        $this->userRepository = $userRepository;
        $this->relationshipRepository = $relationshipRepository;
        $this->profileRepository = $profileRepository;
        $this->em = $doctrine->getManager();
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getFriends(int $userId): array
    {
        $result = [];
        $friends = $this->relationshipRepository->getFriends($userId);
        foreach ($friends as $friend) {
            $result[] = $this->profileRepository->getUserProfile((int) $friend);
        }
        return $result;
    }

    /**
     * @param int $userId
     * @param bool $outcome
     * @return array
     */
    public function getFriendshipRequests(int $userId, bool $outcome = false): array
    {
        return $this->relationshipRepository->getFriendshipRequests(
            $userId,
            $outcome
        );
    }

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return void
     */
    public function addToFriends(int $userInitiatorId, int $userReceiverId): void
    {
        if ($userInitiatorId == $userReceiverId) {
            throw new LogicException('You can\'t add yourself to friends');
        }

        $relationship = $this->relationshipRepository->getUsersRelation(
            $userInitiatorId,
            $userReceiverId
        );
        if ($relationship) {
            throw new LogicException('Users already has friendship request or friendship');
        }

        if (!$this->userRepository->find($userReceiverId)) {
            throw new LogicException('There is no user with this id');
        }

        $relationship = new UserRelationship();
        $relationship->setUserFirst(
            $this->em->getReference(User::class, $userInitiatorId)
        );
        $relationship->setUserSecond(
            $this->em->getReference(User::class, $userReceiverId)
        );
        $relationship->setStatus(UserRelationship::STATUS_FRIENDSHIP_REQUEST);
        $this->relationshipRepository->add($relationship, true);
    }

    /**
     * @param int $userId
     * @param int $userIdToRemove
     * @return void
     */
    public function removeFromFriends(int $userId, int $userIdToRemove): void
    {
        $relationship = $this->relationshipRepository->getUsersRelation(
            $userId,
            $userIdToRemove,
            UserRelationship::STATUS_FRIENDS
        );

        if (!$relationship) {
            throw new LogicException('User not in friends');
        }

        $this->relationshipRepository->remove($relationship, true);
    }

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return void
     */
    public function acceptFriendshipRequest(int $userInitiatorId, int $userReceiverId): void
    {
        $relationship = $this->relationshipRepository->getFriendshipRequest(
            $userInitiatorId,
            $userReceiverId
        );

        if (!$relationship) {
            throw new LogicException('There is no friendship request');
        }

        $relationship->setStatus(UserRelationship::STATUS_FRIENDS);
        $this->em->flush();
    }

    /**
     * @param int $userInitiatorId
     * @param int $userReceiverId
     * @return void
     */
    public function declineFriendshipRequest(int $userInitiatorId, int $userReceiverId): void
    {
        $relationship = $this->relationshipRepository->getUsersRelation(
            $userInitiatorId,
            $userReceiverId,
            UserRelationship::STATUS_FRIENDSHIP_REQUEST
        );

        if (!$relationship) {
            throw new LogicException('There is no friendship request');
        }

        $this->relationshipRepository->remove($relationship, true);
    }
}