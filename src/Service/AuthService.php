<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Entity\UserIp;
use App\Message\SendNewIpEmail;
use App\Repository\UserIpRepository;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\MessageBusInterface;

class AuthService
{
    protected UserIpRepository $userIpRepository;
    protected ObjectManager $em;
    protected MessageBusInterface $bus;

    /**
     * @param UserIpRepository $userIpRepository
     * @param ManagerRegistry $doctrine
     * @param MessageBusInterface $bus
     */
    public function __construct(
        UserIpRepository    $userIpRepository,
        ManagerRegistry     $doctrine,
        MessageBusInterface $bus
    )
    {
        $this->userIpRepository = $userIpRepository;
        $this->em = $doctrine->getManager();
        $this->bus = $bus;
    }

    /**
     * @param string $userEmail
     * @param int $userId
     * @param string $ip
     * @return void
     */
    public function handleLoginIp(string $userEmail, int $userId, string $ip): void
    {
        $dateTimeNow = new DateTimeImmutable();
        $isNewIp = $this->userIpRepository->isNewIp($userId, $ip);
        if (!$isNewIp) {
            /** @var UserIp $userIp */
            $userIp = $this->userIpRepository->findByUserIdAndIp($userId, $ip);
            $userIp->setLastLoginAt($dateTimeNow);
            $this->em->flush();
            return;
        }

        $userIp = new UserIp();
        $userIp->setIp($ip);
        $userIp->setLastLoginAt($dateTimeNow);
        $userIp->setUser($this->em->getReference(User::class, $userId));
        $this->userIpRepository->add($userIp, true);

        $this->bus->dispatch(
            new SendNewIpEmail(
                $userEmail,
                $ip,
                $dateTimeNow
            ),
            [new AmqpStamp('new_ip')]
        );
    }
}