<?php
declare(strict_types=1);

namespace App\Service\User;

use App\Entity\ConfirmationCode;
use App\Entity\User;
use App\Entity\UserProfile;
use App\Message\SendRegistrationConfirmationEmail;
use App\Repository\CacheDecorator\Interfaces\UserProfileRepositoryInterface;
use App\Repository\ConfirmationCodeRepository;
use App\Repository\UserRepository;
use App\Service\User\DTO\Register;
use App\Service\User\DTO\UpdateProfile;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use LogicException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    protected UserRepository $userRepository;
    protected UserProfileRepositoryInterface $profileRepository;
    protected ConfirmationCodeRepository $confirmationCodeRepository;
    protected UserPasswordHasherInterface $passwordHasher;
    protected ObjectManager $em;
    protected MessageBusInterface $bus;
    protected ParameterBagInterface $params;

    /**
     * @param UserRepository $userRepository
     * @param UserProfileRepositoryInterface $profileRepository
     * @param ConfirmationCodeRepository $confirmationCodeRepository
     * @param UserPasswordHasherInterface $passwordHasher
     * @param ManagerRegistry $doctrine
     * @param MessageBusInterface $bus
     * @param ParameterBagInterface $params
     */
    public function __construct(
        UserRepository                 $userRepository,
        UserProfileRepositoryInterface $profileRepository,
        ConfirmationCodeRepository     $confirmationCodeRepository,
        UserPasswordHasherInterface    $passwordHasher,
        ManagerRegistry                $doctrine,
        MessageBusInterface            $bus,
        ParameterBagInterface          $params
    )
    {
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
        $this->confirmationCodeRepository = $confirmationCodeRepository;
        $this->passwordHasher = $passwordHasher;
        $this->em = $doctrine->getManager();
        $this->bus = $bus;
        $this->params = $params;
    }

    /**
     * @param Register $dto
     */
    public function registerUser(Register $dto): void
    {
        $user = new User();
        $user->setEmail($dto->getEmail());

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $dto->getPassword()
        );

        $user->setPassword($hashedPassword);

        $userProfile = new UserProfile();
        $userProfile->setFirstname($dto->getFirstname());
        $userProfile->setSurname($dto->getSurname());
        $userProfile->setLastname($dto->getLastname());
        $userProfile->setUser($user);

        $this->em->persist($userProfile);
        $this->em->persist($user);
        $this->em->flush();

        $this->bus->dispatch(new SendRegistrationConfirmationEmail(
            $user->getId(),
            $dto->getEmail(),
            $this->params->get('email_confirmation_code_expire'),
            $this->params->get('email_confirmation_link'),
        ));
    }

    /**
     * @param string $confirmationCode
     * @return void
     * @throws LogicException
     */
    public function confirmEmail(string $confirmationCode): void
    {
        try {
            /** @var ConfirmationCode $codeEntity */
            $codeEntity = $this->confirmationCodeRepository->getNonUsedByCode(
                ConfirmationCode::TYPE_REGISTRATION,
                $confirmationCode
            );
        } catch (NonUniqueResultException) {
            throw new LogicException('Something went wrong. Please, try again later');
        }

        if (!$codeEntity) {
            throw new LogicException('Something went wrong. Please, try again later');
        }

        $codeEntity->setStatus(ConfirmationCode::STATUS_USED);
        $codeEntity->getUser()->setStatus(User::STATUS_ACTIVE);
        $this->em->flush();
    }

    /**
     * @param int $userId
     * @param string $expirationPeriodRaw
     * @return string
     * @throws Exception
     */
    public function createConfirmationCode(int $userId, string $expirationPeriodRaw): string
    {
        $code = self::createVerificationCode();
        $user = $this->em->getReference(User::class, $userId);
        $expirationPeriod = (new DateTimeImmutable())->add(new DateInterval($expirationPeriodRaw));

        $entity = new ConfirmationCode();
        $entity
            ->setType(ConfirmationCode::TYPE_REGISTRATION)
            ->setCode($code)
            ->setUser($user)
            ->setExpiredAt($expirationPeriod);

        $this->em->persist($entity);
        $this->em->flush();

        return $code;
    }


    /**
     * @param int $userId
     * @param UpdateProfile $dto
     */
    public function updateProfile(int $userId, UpdateProfile $dto): void
    {
        $userProfile = $this->profileRepository->getUserProfile($userId);

        $userProfile->setFirstname($dto->getFirstname());
        $userProfile->setSurname($dto->getSurname());
        $userProfile->setLastname($dto->getLastname());

        $this->profileRepository->add($userProfile, true);
    }

    /**
     * @return string
     */
    private static function createVerificationCode(): string
    {
        return md5(uniqid((string)rand(), true));
    }
}