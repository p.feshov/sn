<?php
declare(strict_types=1);

namespace App\Service\User\DTO;

use App\ArgumentResolver\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateProfile implements RequestDTOInterface
{
    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 50,
    )]
    private ?string $firstname = null;

    #[Assert\Length(
        max: 50,
    )]
    private ?string $surname = null;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 50,
    )]
    private ?string $lastname = null;

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }
}