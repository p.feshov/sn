<?php
declare(strict_types=1);

namespace App\Service\User\DTO;

use App\ArgumentResolver\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Register implements RequestDTOInterface
{
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Assert\Length(
        max: 255,
        maxMessage : "Email cannot be longer than {{ limit }} characters"
    )]
    private ?string $email = null;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 8,
        max: 50,
        minMessage: "Password must be at least {{ limit }} characters long",
        maxMessage : "Password cannot be longer than {{ limit }} characters"
    )]
    private ?string $password = null;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 50,
    )]
    private ?string $firstname = null;

    #[Assert\Length(
        max: 50,
    )]
    private ?string $surname = null;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 50,
    )]
    private ?string $lastname = null;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Register
     */
    public function setEmail(?string $email): Register
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return Register
     */
    public function setPassword(?string $password): Register
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * @return Register
     */
    public function setFirstname(?string $firstname): Register
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return Register
     */
    public function setSurname(?string $surname): Register
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return Register
     */
    public function setLastname(?string $lastname): Register
    {
        $this->lastname = $lastname;
        return $this;
    }
}