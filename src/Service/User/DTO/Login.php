<?php
declare(strict_types=1);

namespace App\Service\User\DTO;

use App\ArgumentResolver\RequestDTOInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Login implements RequestDTOInterface
{
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Assert\Length(
        max: 255,
        maxMessage : "Email cannot be longer than {{ limit }} characters"
    )]
    private ?string $email = null;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 8,
        max: 50,
        minMessage: "Password must be at least {{ limit }} characters long",
        maxMessage : "Password cannot be longer than {{ limit }} characters"
    )]
    private ?string $password = null;

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
}