<?php
declare(strict_types=1);

namespace App\Service\Post\DTO;

use App\ArgumentResolver\RequestDTOInterface;
use App\Entity\Post as PostEntity;
use DateTimeImmutable;
use Symfony\Component\Validator\Constraints as Assert;

class Post implements RequestDTOInterface
{
    #[Assert\NotBlank]
    #[Assert\Choice(choices: [PostEntity::PRIVACY_LEVEL_ALL, PostEntity::PRIVACY_LEVEL_FRIENDS])]
    private int $privacyLevel;

    #[Assert\NotBlank]
    #[Assert\Length(
        min: 2,
        max: 50000,
    )]
    private ?string $content = null;

    #[Assert\Type("\DateTimeInterface")]
    private ?DateTimeImmutable $publishedAt = null;

    /**
     * @return int
     */
    public function getPrivacyLevel(): int
    {
        return $this->privacyLevel;
    }

    /**
     * @param int $privacyLevel
     * @return Post
     */
    public function setPrivacyLevel(int $privacyLevel): Post
    {
        $this->privacyLevel = $privacyLevel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return Post
     */
    public function setContent(?string $content): Post
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    /**
     * @param DateTimeImmutable $publishedAt
     * @return Post
     */
    public function setPublishedAt(DateTimeImmutable $publishedAt): Post
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }
}