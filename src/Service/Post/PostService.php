<?php
declare(strict_types=1);

namespace App\Service\Post;

use App\Entity\Post;
use App\Entity\User;
use App\Repository\CacheDecorator\Interfaces\PostLikeRepositoryInterface;
use App\Repository\CacheDecorator\Interfaces\PostRepositoryInterface;
use App\Repository\UserRepository;
use App\Util\Paginator;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use LogicException;

class PostService
{
    protected UserRepository $userRepository;
    protected PostRepositoryInterface $postRepository;
    protected PostLikeRepositoryInterface $postLikeRepository;
    protected ObjectManager $em;

    /**
     * @param UserRepository $userRepository
     * @param PostRepositoryInterface $postRepository
     * @param PostLikeRepositoryInterface $postLikeRepository
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        UserRepository              $userRepository,
        PostRepositoryInterface     $postRepository,
        PostLikeRepositoryInterface $postLikeRepository,
        ManagerRegistry             $doctrine,
    )
    {
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->postLikeRepository = $postLikeRepository;
        $this->em = $doctrine->getManager();
    }

    /**
     * @param int $userId
     * @param int $privacyLevel
     * @param int|null $page
     * @param int|null $pageSize
     * @return Paginator
     */
    public function getUserPostList(
        int $userId,
        int $privacyLevel,
        ?int $page = 1,
        ?int $pageSize = 20
    ): Paginator
    {
        $page = $page ?? 1;
        $pageSize = $pageSize ?? 20;
        $posts = $this->postRepository->findByUserId(
            $userId,
            $privacyLevel,
            ($page - 1) * $pageSize,
            $pageSize
        );

        /** @var Post $post */
        foreach ($posts->getItems() as $post) {
            $post->setLikeCount(
                $this->postLikeRepository->getPostLikesCount($post->getId())
            );
        }

        return $posts;
    }

    /**
     * @param DTO\Post $dto
     * @param int $userId
     */
    public function createPost(DTO\Post $dto, int $userId): void
    {
        $post = new Post();

        $post->setUser($this->em->getReference(User::class, $userId));
        $post->setContent($dto->getContent());
        $post->setPrivacyLevel($dto->getPrivacyLevel());

        $dateTimeNow = new DateTimeImmutable();
        $publishedAt = $dto->getPublishedAt() ?? $dateTimeNow;
        if ($dateTimeNow > $publishedAt) {
            throw new LogicException("Published at cannot be earlier than current datetime");
        }

        $post->setPublishedAt($publishedAt);

        $this->postRepository->add($post, true);
    }

    /**
     * @param int $postId
     * @param int $userId
     * @param DTO\Post $dto
     */
    public function updatePost(int $postId, int $userId, DTO\Post $dto): void
    {
        $post = $this->getPost($postId, $userId);

        $post->setContent($dto->getContent());
        $post->setPrivacyLevel($dto->getPrivacyLevel());

        $this->postRepository->add($post, true);
    }

    /**
     * @param int $postId
     * @param int $userId
     */
    public function deletePost(int $postId, int $userId): void
    {
        $post = $this->getPost($postId, $userId);
        $this->postRepository->remove($post, true);
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return Post|null
     */
    protected function getPost(int $postId, int $userId): ?Post
    {
        $post = $this->postRepository->findByIdAndUserId(
            $postId,
            $userId
        );

        if (!$post) {
            throw new LogicException('Post not found');
        }

        return $post;
    }
}