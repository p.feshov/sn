<?php
declare(strict_types=1);

namespace App\Service\Post;

use App\Entity\Post;
use App\Entity\PostLike;
use App\Entity\User;
use App\Repository\CacheDecorator\Interfaces\PostLikeRepositoryInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use LogicException;

class LikeService
{
    protected PostLikeRepositoryInterface $postLikeRepository;
    protected ObjectManager $em;

    /**
     * @param PostLikeRepositoryInterface $postLikeRepository
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        PostLikeRepositoryInterface $postLikeRepository,
        ManagerRegistry    $doctrine,
    )
    {
        $this->postLikeRepository = $postLikeRepository;
        $this->em = $doctrine->getManager();
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return void
     */
    public function likePost(int $postId, int $userId): void
    {
        $postLike = new PostLike();
        $postLike->setUser($this->em->getReference(User::class, $userId));
        $postLike->setPost($this->em->getReference(Post::class, $postId));

        $this->postLikeRepository->add($postLike, true);
    }

    /**
     * @param int $postId
     * @param int $userId
     * @return void
     */
    public function unlikePost(int $postId, int $userId): void
    {
        $postLike = $this->postLikeRepository->findByPostIdAndUserId(
            $postId,
            $userId
        );
        if (!$postLike) {
            throw new LogicException('User have no like on this post');
        }

        $this->postLikeRepository->remove($postLike, true);
    }
}