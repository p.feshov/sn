<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use App\Service\AuthService;
use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener
{
    protected EntityManager $em;
    protected RequestStack $requestStack;
    protected AuthService $authService;

    public function __construct(
        EntityManager $em,
        RequestStack  $requestStack,
        AuthService   $authService
    )
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->authService = $authService;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();
        $request = $this->requestStack->getCurrentRequest();
        $email = $event->getUser()->getUserIdentifier();

        /** @var User $user */
        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy([
                'email' => $email
            ]);

        $ip = $request->getClientIp();
        $this->authService->handleLoginIp($email, $user->getId(), $ip);

        $payload['id'] = $user->getId();

        $event->setData($payload);
    }
}