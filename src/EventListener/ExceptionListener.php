<?php
declare(strict_types=1);

namespace App\EventListener;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if (!$exception instanceof ValidationException) {
            return;
        }

        $responseData = [
            'status' => 'error',
            'message' => 'Validation error',
            'errors' => json_decode($exception->getMessage())
        ];

        $response = new JsonResponse($responseData);
        $response->setStatusCode($exception->getStatusCode());
        $event->setResponse($response);
    }
}