<?php

namespace App\Controller;

use App\Service\FriendshipService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FriendController extends AbstractController
{
    protected FriendshipService $friendshipService;

    /**
     * @param FriendshipService $friendshipService
     */
    public function __construct(
        FriendshipService $friendshipService
    )
    {
        $this->friendshipService = $friendshipService;
    }

    #[Route('/api/friend/get-user-friends', name: 'friends_get_user_friends', methods: 'get')]
    public function getFriends(): Response
    {
        return $this->json(
            [
                'status' => 'success',
                'data' => $this->friendshipService->getFriends($this->getUser()->getId())
            ],
            200,
            [],
            ['groups' => 'friends']
        );
    }

    #[Route('/api/friend/get-friendship-requests/income', name: 'friends_get_friendship_requests_income', methods: 'get')]
    public function getIncomeFriendshipRequests(): Response
    {
        return $this->json([
            'status' => 'success',
            'data' => $this->friendshipService->getFriendshipRequests($this->getUser()->getId())
        ]);
    }

    #[Route('/api/friend/get-friendship-requests/outcome', name: 'friends_get_friendship_requests_outcome', methods: 'get')]
    public function getOutcomeFriendshipRequests(): Response
    {
        return $this->json([
            'status' => 'success',
            'data' => $this->friendshipService->getFriendshipRequests($this->getUser()->getId(), true)
        ]);
    }

    #[Route('/api/friend/add/{userId}', name: 'friends_add', methods: 'get')]
    public function add(int $userId): Response
    {
        $this->friendshipService->addToFriends($this->getUser()->getId(), $userId);
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/friend/remove/{userId}', name: 'friends_remove', methods: 'get')]
    public function remove(int $userId): Response
    {
        $this->friendshipService->removeFromFriends($this->getUser()->getId(), $userId);
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/friend/accept-request/{userId}', name: 'friends_accept_request', methods: 'get')]
    public function acceptRequest(int $userId): Response
    {
        $this->friendshipService->acceptFriendshipRequest($userId, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/friend/decline-request/{userId}', name: 'friends_decline_request', methods: 'get')]
    public function declineRequest(int $userId): Response
    {
        $this->friendshipService->declineFriendshipRequest($userId, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }
}
