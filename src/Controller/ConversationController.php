<?php

namespace App\Controller;

use App\Service\ConversationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ConversationController extends AbstractController
{
    protected ConversationService $conversationService;
    protected Security $security;

    /**
     * @param ConversationService $conversationService
     * @param Security $security
     */
    public function __construct(ConversationService $conversationService, Security $security)
    {
        $this->conversationService = $conversationService;
        $this->security = $security;
    }

    #[Route('/api/conversation/list', name: 'conversation_list', methods: 'get')]
    public function list(): Response
    {
        return $this->json([
            'status' => 'success',
            'data' => $this->conversationService->getConversationList($this->getUser()->getId())
        ]);
    }

    #[Route('/api/conversation/start/{recipientUserId}', name: 'conversation_start', methods: 'get')]
    public function startConversation(int $recipientUserId): Response
    {
        $this->conversationService->startNewConversation($this->getUser()->getId(), $recipientUserId);
        return $this->json(['status' => 'success']);
    }

    #[Route('/api/conversation/messages/{conversationId}', name: 'conversation_messages', methods: 'get')]
    public function getLastMessages(int $conversationId, Request $request): Response
    {
        $result = $this->conversationService->getMessages(
            $conversationId,
            $request->query->get('page'),
            $request->query->get('pageSize')
        );
        return $this->json([
            'status' => 'success',
            'data' => [
                'items' => $result->getItems(),
                'pageSize' => $result->getPageSize(),
                'page' => $result->getCurrentPage(),
                'totalPage' => $result->getTotalPages(),
            ]
        ]);
    }
}
