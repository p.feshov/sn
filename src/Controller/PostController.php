<?php

namespace App\Controller;

use App\Service\Post\DTO\Post;
use App\Service\Post\LikeService;
use App\Service\Post\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    protected PostService $postService;
    protected LikeService $likeService;

    /**
     * @param PostService $postService
     * @param LikeService $likeService
     */
    public function __construct(
        PostService $postService,
        LikeService $likeService
    )
    {
        $this->postService = $postService;
        $this->likeService = $likeService;
    }

    #[Route('/api/post/create', name: 'post_create', methods: 'post')]
    public function create(Post $dto): Response
    {
        $this->postService->createPost($dto, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/post/update/{id}', name: 'post_update', methods: 'post')]
    public function update(int $id, Post $dto): Response
    {
        $this->postService->updatePost($id, $this->getUser()->getId(), $dto);
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/post/delete/{id}', name: 'post_delete', methods: 'get')]
    public function delete(int $id): Response
    {
        $this->postService->deletePost($id, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/post/get-user-posts/{id}', name: 'post_get_user_posts', methods: 'get')]
    public function getAll(int $id, Request $request): Response
    {
        $result = $this->postService->getUserPostList(
            $id,
            \App\Entity\Post::PRIVACY_LEVEL_ALL,
            $request->query->get('page'),
            $request->query->get('pageSize')
        );
        return $this->json(
            [
                'status' => 'success',
                'data' => [
                    'items' => $result->getItems(),
                    'pageSize' => $result->getPageSize(),
                    'page' => $result->getCurrentPage(),
                    'totalPage' => $result->getTotalPages(),
                ]
            ],
            200,
            [],
            ['groups' => 'user_posts']
        );
    }

    #[Route('/api/post/like/{postId}', name: 'post_like', methods: 'get')]
    public function likePost(int $postId): Response
    {
        $this->likeService->likePost($postId, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }

    #[Route('/api/post/unlike/{postId}', name: 'post_unlike', methods: 'get')]
    public function unlikePost(int $postId): Response
    {
        $this->likeService->unlikePost($postId, $this->getUser()->getId());
        return $this->json([
            'status' => 'success'
        ]);
    }
}
