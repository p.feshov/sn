<?php

namespace App\Controller;

use App\Service\User\DTO\Register;
use App\Service\User\DTO\UpdateProfile;
use App\Service\User\UserService;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    protected UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    #[Route('/api/register', name: 'user_register', methods: 'post')]
    public function register(Register $dto): Response
    {
        $this->userService->registerUser($dto);
        return $this->json(['status' => 'success', 'message' => 'User successfully registered']);
    }

    #[Route('/api/user/confirm-email/{code}', name: 'user_confirm_email', methods: 'get')]
    public function confirmEmail(string $code): Response
    {
        try {
            $this->userService->confirmEmail($code);
            return $this->json(['status' => 'success', 'message' => 'Email successfully confirmed']);
        } catch (LogicException $e) {
            return $this->json(['status' => 'error', 'message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/api/profile/update', name: 'profile_update', methods: 'post')]
    public function updateProfile(UpdateProfile $dto): Response
    {
        $this->userService->updateProfile($this->getUser()->getId(), $dto);
        return $this->json(['status' => 'success', 'message' => 'User successfully registered']);
    }
}
