module messenger

go 1.16

require (
	github.com/go-redis/redis/v9 v9.0.0-beta.1
	github.com/golang-jwt/jwt/v4 v4.4.1
	github.com/gorilla/websocket v1.5.0
	github.com/jackc/pgx/v4 v4.16.1 // indirect
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
)
