package main

import (
	"crypto/rsa"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"messenger/packages/db"
	"messenger/packages/redis"
	"net/http"
	"os"
)

var DB *sqlx.DB
var jwtPublicRsa *rsa.PublicKey

func main() {
	config()

	conn := &wsHandler{}
	http.HandleFunc("/", auth(conn, conn.serveWs))

	err := http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("SERVER_PORT")), nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func config() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	DB = db.ConnectDB()
	redis.Client = redis.ConnectRedis()

	signBytes, _ := ioutil.ReadFile("jwt/public.pem")
	jwtPublicRsa, err = jwt.ParseRSAPublicKeyFromPEM(signBytes)

	if err != nil {
		log.Fatal("Error loading public.pem file")
	}
}

func auth(conn *wsHandler, h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			http.NotFound(w, r)
			return
		}

		var userToken string
		if userToken = r.Header.Get("X-Auth-Token"); userToken == "" {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}

		token, err := jwt.Parse(userToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return jwtPublicRsa, nil
		})

		if err != nil {
			log.Println(err)
			http.Error(w, "", http.StatusUnauthorized)
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			conn.userId = int(claims["id"].(float64))
			h.ServeHTTP(w, r)
		} else {
			log.Println(err)
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
	}
}
