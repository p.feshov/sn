package main

import (
	"encoding/json"
	"log"
	"messenger/packages/service"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait      = time.Minute * 10
	maxMessageSize = 512
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true // Disable CORS for testing
	},
}

type Client struct {
	userId         int
	send           chan *service.WsMessage
	conn           *websocket.Conn
	messageService *service.MessageService
}

func (c *Client) read() {
	c.conn.SetReadLimit(maxMessageSize)
	for {
		_, messageData, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				_ = c.conn.Close()
				return
			}
			log.Println("Read message:", err)
			break
		}

		wsMessage := service.WsMessage{}
		if err := json.Unmarshal(messageData, &wsMessage); err != nil {
			log.Println("Unmarshal message:", err)
			continue
		}

		c.sendMessage(wsMessage.Type, wsMessage.ConversationId, wsMessage.Data)
	}
}

func (c *Client) write() {
	for {
		select {
		case messageRaw, ok := <-c.send:
			message, err := json.Marshal(messageRaw)
			if err != nil {
				log.Println("HandleMessage", err)
				return
			}

			err = c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err != nil {
				log.Println("SetWriteDeadline:", err)
				_ = c.conn.Close()
				return
			}

			if !ok {
				_ = c.conn.Close()
				return
			}

			err = c.conn.WriteMessage(websocket.TextMessage, message)
			if err != nil {
				log.Println("Write message:", err)
				_ = c.conn.Close()
				return
			}
		}
	}
}

func (c *Client) sendMessage(messageType, conversationId int, messageData map[string]interface{}) {
	var err error
	switch messageType {
	case service.TypeOpenConversation:
		err = c.messageService.StartConversation(conversationId)
	case service.TypeCloseConversation:
		c.messageService.CloseConversation(conversationId)
	case service.TypeTextMessage:
		err = c.messageService.SendMessage(conversationId, messageData["text"].(string))
	case service.TypeReadMessage:
		c.messageService.ReadMessage(conversationId, int(messageData["messageId"].(float64)))
	}

	if err != nil {
		c.messageService.SendErrorMessage(conversationId, err)
	}
}

type wsHandler struct {
	userId int
}

func (ws *wsHandler) serveWs(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
		return
	}

	channel := make(chan *service.WsMessage)

	client := &Client{
		conn:           conn,
		send:           channel,
		userId:         ws.userId,
		messageService: service.GetNewMessageService(ws.userId, &channel, DB),
	}

	go client.read()
	go client.write()
}
