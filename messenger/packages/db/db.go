package db

import (
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	"os"
)

type User struct {
	IsOnline bool `db:"is_online"`
}

type UserConversation struct {
	UserId         int `db:"user_id"`
	ConversationId int `db:"conversation_id"`
}

type Message struct {
	Id             int
	SenderId       int `db:"sender_id"`
	ConversationId int `db:"conversation_id"`
	Status         int
}

func ConnectDB() *sqlx.DB {
	db, err := sqlx.Connect("pgx", os.Getenv("DB"))
	if err != nil {
		panic(err.Error())
	}

	return db
}
