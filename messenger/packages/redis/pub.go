package redis

import (
	"context"
	"github.com/go-redis/redis/v9"
	"log"
	"os"
	"strconv"
)

var Client *redis.Client

func ConnectRedis() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:        os.Getenv("REDIS"),
		Password:    os.Getenv("REDIS_PASSWORD"),
		ReadTimeout: -1,
	})

	_, err := client.Ping(context.Background()).Result()
	if err != nil {
		log.Println(err)
		return nil
	}

	return client
}

type Publisher struct {
	conn *redis.Client
}

func InitPublisher() *Publisher {
	client := &Publisher{
		conn: Client,
	}

	return client
}

func (p *Publisher) SendMessage(channelName string, message []byte) {
	err := p.conn.Publish(context.Background(), channelName, message).Err()
	if err != nil {
		panic(err)
	}
}

func GetChannelName(conversationId int) string {
	return strconv.Itoa(conversationId)
}
