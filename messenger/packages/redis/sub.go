package redis

import (
	"context"
	"github.com/go-redis/redis/v9"
	"log"
)

type Subscriber struct {
	conn           *redis.Client
	sub            *redis.PubSub
	messageHandler MessageHandler
	channelName    string
	stop           chan struct{}
}

type MessageHandler interface {
	HandleMessage(message string)
}

func InitSubscriber(channelName string, handler MessageHandler) *Subscriber {
	client := &Subscriber{
		channelName:    channelName,
		messageHandler: handler,
		conn:           Client,
	}

	client.sub = client.conn.Subscribe(context.TODO(), channelName)
	return client
}

func (s *Subscriber) HandleMessages() {
	ch := s.sub.Channel()

	for messageData := range ch {
		s.messageHandler.HandleMessage(messageData.Payload)
	}
}

func (s *Subscriber) Destroy() {
	err := s.sub.Unsubscribe(context.TODO(), s.channelName)
	if err != nil {
		log.Println("Subscriber.Destroy:", err)
	}
}
