package db

import (
	"github.com/jmoiron/sqlx"
)

type UserConversationRepository struct {
	db *sqlx.DB
}

func InitUserConversationRepository(db *sqlx.DB) *UserConversationRepository {
	return &UserConversationRepository{
		db: db,
	}
}

func (r UserConversationRepository) IsUserParticipant(conversationId, userId int) (bool, error) {
	var result bool
	query := `SELECT EXISTS(SELECT FROM conversation_user cu WHERE cu.conversation_id = $1 AND cu.user_id = $2)`
	err := r.db.Get(&result, query, conversationId, userId)

	if err != nil {
		return false, err
	}

	return result, nil
}

func (r UserConversationRepository) GetUserIdsInConversation(conversationId, senderId int) ([]int64, error) {
	var userIds []int64

	query := `SELECT conversation_user.user_id from conversation_user where conversation_user.conversation_id = $1 and conversation_user.user_id != $2`
	err := r.db.Select(&userIds, query, conversationId, senderId)

	if err != nil {
		return nil, err
	}

	return userIds, nil
}
