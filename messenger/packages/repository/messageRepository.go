package db

import (
	"github.com/jmoiron/sqlx"
	"log"
)

const (
	StatusUnread = 1
	StatusRead   = 2
)

type MessageRepository struct {
	db *sqlx.DB
}

func InitMessageRepository(db *sqlx.DB) *MessageRepository {
	return &MessageRepository{
		db: db,
	}
}

func (r MessageRepository) Create(senderId, conversationId int, text string) (int, error) {
	var id int
	query := `INSERT INTO message(sender_id, conversation_id, text, status) VALUES($1, $2, $3, $4) RETURNING id`
	err := r.db.Get(&id, query, senderId, conversationId, text, StatusUnread)
	if err != nil {
		log.Println("MessageRepository.Create: ", err)
	}

	return id, err
}

func (r MessageRepository) Read(readerId, conversationId, messageId int) error {
	query := `UPDATE message SET status = $1 WHERE sender_id <> $2 AND conversation_id = $3 AND id = $4`
	_, err := r.db.Exec(query, StatusRead, readerId, conversationId, messageId)
	if err != nil {
		log.Println("MessageRepository.Read: ", err)
	}

	return err
}
