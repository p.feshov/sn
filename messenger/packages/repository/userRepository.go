package db

import (
	"github.com/jmoiron/sqlx"
	"messenger/packages/db"
)

type UserRepository struct {
	db *sqlx.DB
}

func InitUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (r UserRepository) GetUser(userId int) (*db.User, error) {
	var user db.User

	query := `SELECT user.* from user where user.id = $1`
	err := r.db.Get(&user, query, userId)

	if err != nil {
		return nil, err
	}

	return &user, nil
}
