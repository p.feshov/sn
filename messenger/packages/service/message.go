package service

import (
	"encoding/json"
	"errors"
	"github.com/jmoiron/sqlx"
	"log"
	"messenger/packages/redis"
	db "messenger/packages/repository"
)

const TypeOpenConversation = 0
const TypeCloseConversation = 1
const TypeTextMessage = 2
const TypeReadMessage = 3
const TypeError = 4

type Message struct {
	MessageType    int
	ConversationId int
	SenderId       int
	Data           map[string]interface{}
}

func (m *Message) MarshalBinary() ([]byte, error) {
	return json.Marshal(m)
}

type WsMessage struct {
	ConversationId int                    `json:"conversationId"`
	Type           int                    `json:"type"`
	Data           map[string]interface{} `json:"data"`
	Error          string                 `json:"error"`
}

type IncomeMessageHandler struct {
	userId        int
	clientChannel *chan *WsMessage
}

func (h *IncomeMessageHandler) HandleMessage(messagePayload string) {
	message := Message{}
	if err := json.Unmarshal([]byte(messagePayload), &message); err != nil {
		log.Println("MessageService.HandleMessage:", err)
	}

	// Не отправляем сообщения от юзера самому себе
	if message.SenderId == h.userId {
		return
	}

	switch message.MessageType {
	case TypeTextMessage, TypeReadMessage, TypeError:
		msg := &WsMessage{
			ConversationId: message.ConversationId,
			Type:           message.MessageType,
			Data:           message.Data,
		}

		*h.clientChannel <- msg
	}
}

type MessageService struct {
	senderId               int
	clientChannel          *chan *WsMessage
	pub                    *redis.Publisher
	sub                    map[int]*redis.Subscriber
	dbClient               *sqlx.DB
	messageRepository      *db.MessageRepository
	conversationRepository *db.UserConversationRepository
}

func GetNewMessageService(senderId int, clientChannel *chan *WsMessage, dbClient *sqlx.DB) *MessageService {
	return &MessageService{
		senderId:               senderId,
		clientChannel:          clientChannel,
		pub:                    redis.InitPublisher(),
		sub:                    make(map[int]*redis.Subscriber),
		dbClient:               dbClient,
		messageRepository:      db.InitMessageRepository(dbClient),
		conversationRepository: db.InitUserConversationRepository(dbClient),
	}
}

func (m *MessageService) StartConversation(conversationId int) error {
	if m.sub[conversationId] != nil {
		return nil
	}

	isUserParticipant, err := m.conversationRepository.IsUserParticipant(conversationId, m.senderId)
	if !isUserParticipant || err != nil {
		return errors.New("conversation not found")
	}

	sub := redis.InitSubscriber(redis.GetChannelName(conversationId), &IncomeMessageHandler{
		userId:        m.senderId,
		clientChannel: m.clientChannel,
	})
	go sub.HandleMessages()
	m.sub[conversationId] = sub
	return nil
}

func (m *MessageService) CloseConversation(conversationId int) {
	if m.sub[conversationId] != nil {
		m.sub[conversationId].Destroy()
		delete(m.sub, conversationId)
	}
}

func (m *MessageService) SendMessage(conversationId int, messageData string) error {
	messageId, err := m.messageRepository.Create(m.senderId, conversationId, messageData)
	if err != nil {
		return errors.New("error while sending message")
	}

	m.sendMessageToRedis(
		TypeTextMessage,
		conversationId,
		map[string]interface{}{
			"id":   messageId,
			"text": messageData,
		},
	)

	return nil
}

func (m *MessageService) ReadMessage(conversationId int, messageId int) {
	if err := m.messageRepository.Read(m.senderId, conversationId, messageId); err != nil {
		return
	}

	m.sendMessageToRedis(
		TypeReadMessage,
		conversationId,
		map[string]interface{}{
			"id": messageId,
		},
	)
}

func (m *MessageService) SendErrorMessage(conversationId int, err error) {
	msg := &WsMessage{
		ConversationId: conversationId,
		Type:           TypeError,
		Error:          err.Error(),
	}

	*m.clientChannel <- msg
}

func (m *MessageService) sendMessageToRedis(messageType, conversationId int, messageData map[string]interface{}) {
	message := Message{
		MessageType:    messageType,
		ConversationId: conversationId,
		SenderId:       m.senderId,
		Data:           messageData,
	}
	messageBinary, err := message.MarshalBinary()
	if err != nil {
		log.Println("MessageService.sendMessageToRedis:", err)
		return
	}

	m.pub.SendMessage(redis.GetChannelName(conversationId), messageBinary)
}
