#!/bin/bash
set -e

composer install
php bin/console doctrine:migrations:migrate

sleep 5

php bin/console messenger:consume emailNotifications -vv &

exec $@